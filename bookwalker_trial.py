import sys
import os
import requests
from pyquery import PyQuery
from urllib.parse import urlparse, parse_qs, urlencode
import numpy as np
from pathlib import Path

def get_cid(url):
  response = requests.get(f'{url}?sample=1')
  return parse_qs(urlparse(response.request.url).query)['cid'][0]

def get_authorization_headers(url):
  cid = get_cid(url)
  response = requests.get(f'https://viewer-trial.bookwalker.jp/trial-page/c?cid={cid}&BID=xuy20cm');
  return response.json();

def validate_bookwalker_url(url):
  if ("https://bookwalker.jp/" not in url):
    return 'Ошибка: Переданный параметр не является ссылкой на Bookwalker'

  return None

def get_toc(trial_url, auth_query):
  data = requests.get(f'{trial_url}/configuration_pack.json?{auth_query}').json()
  contents = []
  for page in data['configuration']['contents']:
    count = data[page['file']]['FileLinkInfo']['PageCount']
    contents.append({"count": count, "page": page['file'], "array": np.arange(0, int(count), 1)})

  return contents

def download_pages(trial_url, auth_query, toc):
  def encode_url(page, image):
    query = f'{trial_url}/{page}/{image}.jpeg?{auth_query}'
    query = query.replace('/normal_default/..', '')
    query = query.replace('normal_default/', '')
    if ("fmatter" in page):
      query = query.replace('SVGA/item/', 'SVGA/shared/item/')
    else:
      query = query.replace('SVGA/item/', 'SVGA/normal_default/item/')
    return query

  save_folder = './pages'
  if (os.path.exists(save_folder) != True):
    os.mkdir(save_folder)
  [f.unlink() for f in Path(save_folder).glob("*") if f.is_file()]

  count = len(toc)
  i = 0
  fileIndex = 0
  for item in toc:
    for number in item['array']:
      current_url = encode_url(item['page'], number)
      file = requests.get(current_url)
      open(f'{save_folder}/{fileIndex}.jpeg', 'wb').write(file.content)
      fileIndex = fileIndex + 1
      print(f'Скачиваю...{i}/{count} блок: {number + 1}/{item["count"]} страниц')

    i = i + 1
  return

if __name__ == "__main__":
  if len(sys.argv) < 2:
    print(f'Не передан параметр\nПример: py {os.path.basename(__file__)} https://bookwalker.jp/de5c6d5b38-ba76-4ad2-bc67-06c3013256a7')
    exit()
  
  url = sys.argv[1][:-1] if sys.argv[1][-1] == '/' else sys.argv[1]
  is_valid_url = validate_bookwalker_url(url);
  if is_valid_url is not None:
    print(is_valid_url)
    exit()

  response = requests.get(url)
  if response.status_code != 200:
    print("Ошибка: неправильная ссылка")
    exit()

  print("Получение информации о книге...", end='\r')
  htmlBook = response.text
  pq = PyQuery(htmlBook)

  title = pq(".p-main__title").html()
  if (title is None):
    print("Ошибка: ссылка ведет не на книгу")
    exit()

  print(f'Название: {title}\nПолучение авторизации...', end='\r')
  auth_headers = get_authorization_headers(url)
  print(f'Авторизация получена, парсим TOC...')

  trial_url = f'{auth_headers["url"]}normal_default'
  auth_query = urlencode(auth_headers['auth_info'])

  toc = get_toc(trial_url, auth_query)
  download_pages(trial_url, auth_query, toc)
