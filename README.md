# Выкачка демки
1. Скачиваем python отсюда: https://www.python.org/ftp/python/3.10.6/python-3.10.6-amd64.exe
2. В самом первом окне установки жмем галку "Add python 3.10 to PATH"
![alt text](https://i.imgur.com/DIDjRBf.png)
3. Запускаем ``!pre.bat``
4. Удаляем ``!pre.bat``
5. Вставляем в ``start.bat`` ссылку на книгу, демку которой нам надо скачать, и запускаем.
![alt text](https://i.imgur.com/5y6xgbZ.png)
6. В папке ``pages`` будут скаченные изображения